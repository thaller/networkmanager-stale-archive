#!/bin/bash

set -e

TAG="${TAG-}"
REMOTE="${REMOTE:-origin}"
REMOTE_GIT="${REMOTE_GIT-$(git remote get-url --push "$REMOTE")}"
REMOTE_BACKUP_GIT="${REMOTE_BACKUP_GIT-git@gitlab.freedesktop.org:thaller/NetworkManager-stale-archive.git}"
VERBOSE="${VERBOSE-0}"
DELETE_REF_REMOTE="${DELETE_REF_REMOTE-0}"
DELETE_REF_LOCAL="${DELETE_REF_LOCAL-0}"
ONLY_CLEAN_LOCAL="${ONLY_CLEAN_LOCAL-0}"
ONLY_CLEAN_REMOTE_BACKUP="${ONLY_CLEAN_REMOTE_BACKUP-0}"

show_conf() {
    printf '%s\n' "${1}REMOTE=\"$REMOTE\""
    printf '%s\n' "${1}REMOTE_GIT=\"$REMOTE_GIT\""
    printf '%s\n' "${1}REMOTE_BACKUP_GIT=\"$REMOTE_BACKUP_GIT\""
    printf '%s\n' "${1}TAG=\"$TAG\""
    printf '%s\n' "${1}VERBOSE=\"$VERBOSE\""
    printf '%s\n' "${1}DELETE_REF_REMOTE=\"$DELETE_REF_REMOTE\""
    printf '%s\n' "${1}DELETE_REF_LOCAL=\"$DELETE_REF_LOCAL\""
    printf '%s\n' "${1}ONLY_CLEAN_LOCAL=\"$ONLY_CLEAN_LOCAL\""
    printf '%s\n' "${1}ONLY_CLEAN_REMOTE_BACKUP=\"$ONLY_CLEAN_REMOTE_BACKUP\""
}

usage() {
    printf 'usage: %s [--tag TAG | --auto-tag] [ -v | --verbose ] [ --only-clean-local | --only-clean-remote-backup ]\n' "$0"
    printf '\n'
    printf '  environment:\n'
    show_conf '    '
    printf '\n'
    printf 'This script is to backup/archive the git-refs from our main repository.\n'
    printf 'It fetches all refs from $REMOTE_GIT and pushes them to $REMOTE_BACKUP_GIT\n'
    printf 'It remaps those refs to refs/archive/$TAG/*\n'
    printf 'Afterwards it deletes all the heads from $REMOTE_GIT, except certain protected\n'
    printf 'branches like `main`\n'
    printf '\n'
    printf '  ONLY_CLEAN_LOCAL: only delete refs refs/archive/$TAG/* in local repository\n'
    printf '  ONLY_CLEAN_REMOTE_BACKUP: only delete refs refs/archive/$TAG/* in $REMOTE_BACKUP_GIT repository\n'
}

die() {
    printf '%s\n' "$@" >&2
    exit 1
}

cmd() {
    printf 'EXEC: %s\n' "$*"
    if [ "$VERBOSE" = 1 ]; then
        "$@"
    else
        "$@" &>/dev/null
    fi
}

is_protected_ref() {
    case "$1" in
        refs/heads/main | \
        refs/heads/systemd | \
        refs/heads/nm-1-* )
            ;;
        refs/heads/* )
            return 1
            ;;
        *)
            ;;
    esac

    return 0
}

OLD_IFS="$IFS"

CUR_DIR="$(readlink -f .)"
GIT_DIR="$(git rev-parse --show-toplevel)"

ARGV=( "$@" )
i=0
while test "$i" -lt ${#ARGV[@]} ; do
    C="${ARGV[$i]}"
    i=$((i+1))
    case "$C" in
        --auto-tag)
            TAG="nm-$(date '+%Y%m%d-%H%M%S')"
            ;;
        --tag)
            C="${ARGV[$i]}"
            i=$((i+1))
            test -n "$C" || die "missing argument to --tag option"
            TAG="$C"
            ;;
        -v|--verbose)
            VERBOSE=1
            ;;
        --only-clean-local)
            ONLY_CLEAN_LOCAL=1
            ;;
        --only-clean-remote-backup)
            ONLY_CLEAN_REMOTE_BACKUP=1
            ;;
        -h|--help)
            usage
            exit 0
            ;;
        *)
            usage
            echo
            die "invalid parameter \"$C\""
            ;;
    esac
done

if test -z "$TAG" ; then
    usage
    die "Missing tag."
fi

re='^[a-zA-Z0-9._-]+$'
[[ "$TAG" =~ $re ]] || die "Invalid tag \"$TAG\""

show_conf ''

cd "$GIT_DIR"

if [ "$ONLY_CLEAN_REMOTE_BACKUP" == 1 ] ; then
    IFS=$'\n'
    REFS_LINES=( $(git ls-remote --refs "$REMOTE_BACKUP_GIT") )
    IFS="$OLD_IFS"

    ARGS=()
    for line in "${REFS_LINES[@]}"; do
        SHA="${line%%	*}"
        REF="${line#*	}"

        [[ "$REF" != "refs/archive/$TAG/"* ]] && continue

        echo ">>> [$SHA]    [$REF]"
        ARGS+=("--force-with-lease=$REF:$SHA" ":$REF")
    done
    if [ ${#ARGS[@]} -gt 0 ]; then
        cmd git push "$REMOTE_BACKUP_GIT" "${ARGS[@]}"
    fi
    exit 0
fi

if [ "$ONLY_CLEAN_LOCAL" != 1 ] ; then
    cmd git fetch "$REMOTE_GIT" \
        --no-tags \
        --prune \
        --force \
        "refs/*:refs/archive/$TAG/refs/*" \
        "^refs/archive/*"
fi

if [ "$ONLY_CLEAN_LOCAL" != 1 ] ; then
    cmd git push "$REMOTE_BACKUP_GIT" \
        --no-tags \
        --prune \
        --force \
        "refs/archive/$TAG/*:refs/archive/$TAG/*"
fi

IFS=$'\n'
REFS_LINES=( $(git show-ref) )
IFS="$OLD_IFS"

if [ "$ONLY_CLEAN_LOCAL" != 1 ] ; then
    for line in "${REFS_LINES[@]}"; do
        SHA="${line%% *}"
        REF="${line#* }"

        [[ "$REF" != "refs/archive/$TAG/"* ]] && continue

        re='^refs/archive/([^/]+)/(.*)$'
        [[ "$REF" =~ $re ]] || die bug
        REMOTE_REF="${BASH_REMATCH[2]}"

        [ "$REF" = ${BASH_REMATCH[0]} ] || die bug
        [ "$TAG" = ${BASH_REMATCH[1]} ] || die bug
        [ "$REF" = "refs/archive/$TAG/$REMOTE_REF" ] || die bug

        DEL_REMOTE=0
        is_protected_ref "$REMOTE_REF" || DEL_REMOTE=1

        CMD=(git push "$REMOTE_GIT" --force-with-lease="$REMOTE_REF:$SHA" ":$REMOTE_REF")
        if [ "$DEL_REMOTE" = 1 ]; then
            if [ "$DELETE_REF_REMOTE" = 1 ]; then
                cmd "${CMD[@]}"
            else
                echo "# would delete remote:   ${CMD[@]}"
            fi
        else
            echo "# keep protected remote: ${CMD[@]}"
        fi

    done
fi

for line in "${REFS_LINES[@]}"; do
    SHA="${line%% *}"
    REF="${line#* }"

    [[ "$REF" != "refs/archive/$TAG/"* ]] && continue

    CMD=(git update-ref -d "$REF" "$SHA")
    if [ "$DELETE_REF_LOCAL" = 1 ]; then
        cmd "${CMD[@]}"
    else
        echo "# would delete local refs: ${CMD[@]}"
    fi
done

