NetworkManager-stale-archive
============================

Backup/archive for old WIP references from the
NetworkManager git repository.

For the upstream NetworkManager repository we like to push WIP development
branches. Those are supposed to be deleted after the work is done. However,
that often gets forgotten and the work piles up. Or the work stays unfinished
indefinitely.

The script [archive.sh](archive.sh) can be used to archive
and cleanup the main NetworkManager repository.

It works by fetching all refs from NetworkManager and pushing them to
"refs/archive/$TAG/". Then the refs are deleted from NetworkManager, except
a few protected branches like `main`.
